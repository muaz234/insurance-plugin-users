
<!-- <section class="panel panel-default bg-white m-t-lg">  -->
<!-- <div class="text-center" style="background-color: #d8d8d8;">
		<a href="<?php // echo strip_tags(Configure::read('RCMS.landing_url')); ?>">
			<img src="<?php echo $this->webroot . 'theme/Inspinia/img/prasarana_ico.png'; ?>" class="m-r-sm" width="27" height="27"><img src="<?php echo $this->webroot . 'theme/Inspinia/img/prasarana_logo_icon.png'; ?>" class="m-r-sm" width="150" height="27">
		</a>
</div> -->
<div class="row">
	<div class="col-lg-4">

	</div> 

	<div class="col-lg-4" style="align-items:center; padding-top: 5% ;">
				<?php  //echo json_encode($this->request->params['action']);  ?>
	
		<div class="panel panel-default bg-white m-t-lg">
		
			<header class="panel-heading text-center">

			<div class="text-center">
				<!-- <a href="<?php//echo strip_tags(Configure::read('RCMS.landing_url')); ?>"> -->
					<img src="<?php echo $this->webroot . 'theme/Inspinia/img/prasarana_ico.png'; ?>" class="m-r-sm" width="27" height="27"><img src="<?php echo $this->webroot . 'theme/Inspinia/img/prasarana_logo_icon.png'; ?>" class="m-r-sm" width="150" height="27">
				</a>
			</div>
	  
			</header> 
		
				<?php if (!$this->Session->check('Auth.User.id')) { ?>
			<div class="panel wrapper">
			<?php echo $this->Form->create('User', array(
			'url' => array('plugin' => 'users', 'controller' => 'users', 'action' => 'login'),
			'class' => 'login-form'
			));
			?>

				<div class="panel-body">
					<!-- <div class="ibox-float-e-margins"> -->
					<?php
				$this->Form->inputDefaults(array(
					'label' => false,
				));
				?>
					<div class="form-group ">
						<?php
					echo $this->Form->input('username', array(
						'placeholder' => __d('croogo', 'Username'),
							// 'style' => 'width:537px',
						'class' => 'form-control input-lg',
						'label' => 'Username'
					));
					?>
					</div>

					<div class="form-group">
						<?php
					echo $this->Form->input('password', array(
						'placeholder' => 'Password',
						// 'style' => 'width:537px',
						'class' => 'form-control input-lg',
						'label'=> 'Password',
					));
					?>
						</div>

						<div class="form-group checkbox-inline move-right" style="">
							<?php
						if (Configure::read('Access Control.autoLoginDuration')) :
							echo $this->Form->input('remember', array(
							'label' => __d('croogo', 'Remember me?'),
							'type' => 'checkbox',
							'default' => false,
						));
						endif;
						?>
						</div>
						
						<div class="form-group">
							<!-- <div class="text-center"> -->
							<!-- <div class="btn-group"> -->
							<?php
						echo $this->Form->button(__d('croogo', 'Sign In'), array('class' => 'btn btn-primary btn-lg btn-block ', 'style' => 'margin-left: 0'));  ?>
						</div>
						<div class="form-group">
						<?php
						echo $this->Html->link(__d('croogo', 'Forgot password?'), array(
							'controller' => 'users', 'action' => 'forgot'
						), array('class' => 'btn btn-default btn-lg btn-block  ', 'style' => 'margin: 0 auto;'));
						?>
					</div>
							<!-- </div> -->
						</div>
					<!-- </div> -->

					<footer id="footer">
						<div class="text-center padder">
							<p>
							<!-- <small>ITD In-house Development Team<br>&copy; 2015</small> -->
							<small>© 2018, Prasarana Malaysia Berhad. All Rights Reserved. <br>Best viewed with Google Chrome using 1366 X 768 resolution.<br>
								Developed and hosted by ICT Software Development Team.</small>
							</p>
						</div>
					</footer>
				</div>
				<?php echo $this->Form->end(); ?>
				<?php 
		} else { ?>

				<header class="panel-heading text-center wrapper">
					<strong>Sorry...</strong>
				</header>
				<div class="panel wrapper">
					<div class="box">
						<div class="box-content">
							<center><p> You are not allowed to access this module. <br/>	Please click <a href="<?php echo $this->webroot . 'quotation/applicants'; ?>"<i class="btn btn-default fa fa-home bg-warning"></i> Home</a> to return. </p></center>
						</div>
					</div>
				</div>
				<?php 
		} ?>
				<!-- footer -->
			<!-- </section> -->
			</div>
		</div>
 


		</div>
		<div class="col-lg-4">

		</div>

	</div>
		<!-- </section> -->





